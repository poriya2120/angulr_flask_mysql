import {injectable} from '@angular/core'
import {httpClient} from '@angular/common/http'
improt {observable} from 'rxjs'
import {Task } from './task'

@injectable()
export class TaskService{
    constructor(private http:HttpClient){}

    getTask():observable<Task[]>{
        return this.http.get<Task[]>('api/tasks')
    }

    addTask(task:Task):observable<Task>{
        console.log(task)
        return this.http.post<Task>('api/task',task)
    }
    deleteTask(id:number):observable<{}>{
        const url=`api/task/${id}`
        return this.http.delete(url)
    }

    updateTask(task:Task):observable<{}>{
        const url=`api/task/${id}`
        return this.http.put<Task>(url,task)
    }
}